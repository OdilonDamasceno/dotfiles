" Plugins
call plug#begin('~/.vim/plugged')

Plug 'junegunn/fzf'
Plug 'joshdick/onedark.vim'
Plug 'junegunn/fzf.vim'
Plug 'preservim/nerdtree'
Plug 'dart-lang/dart-vim-plugin'
Plug 'thosakwe/vim-flutter'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'natebosch/vim-lsc'
Plug 'natebosch/vim-lsc-dart'
Plug 'sainnhe/sonokai'
Plug 'wadackel/vim-dogrun'
Plug 'ryanoasis/vim-devicons'
Plug 'arzg/vim-colors-xcode'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
Plug 'lordm/vim-browser-reload-linux'
Plug 'xolox/vim-lua-ftplugin'
Plug 'xolox/vim-misc'
Plug 'sheerun/vim-polyglot'
Plug 'preservim/nerdtree' |
            \ Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'rust-lang/rust.vim'

call plug#end()

colorscheme xcodedark

" KeyBindings
map <C-b> :NERDTreeToggle<CR>
map <C-S-i> :DartFmt<CR>
map <C-n> :bprevious<CR>
map <C-m> :bnext<CR>
map <C-S-p> :source ~/vim-scripts/terminal.vim<CR>

if has('nvim')
	  inoremap <silent><expr> <c-space> coc#refresh()
	else
	  inoremap <silent><expr> <c-@> coc#refresh()
endif
nmap <silent> gi <Plug>(coc-implementation)

" Sets
set tabstop=2
set laststatus=2
set encoding=UTF-8
set number
set splitbelow
set wildmenu



" Variables
let g:lsc_auto_map = v:true
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='deus'
let vim_markdown_preview_github=1
let g:mkdp_browser = 'surf'
let g:bracey_browser_command = 'surf'
let g:lua_compiler_name = '/usr/local/bin/luac'
let g:javascript_plugin_jsdoc = 1
let g:rustfmt_autosave = 1
let NERDTreeShowHidden=1

